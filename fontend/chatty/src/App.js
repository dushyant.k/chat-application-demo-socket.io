import "./App.css";
import { useEffect, useState } from "react";
import { nanoid } from "nanoid";
import io from "socket.io-client";

// No Dotenv
const socket = io.connect("http://localhost:6464/");
const userName = nanoid(4);
function App() {
  const [message, setMessage] = useState("");
  const [chat, setChat] = useState([]);
  const sendChat = (e) => {
    e.preventDefault();
    socket.emit("chat", { message, userName });
    setMessage("");
  };

  useEffect(() => {
    socket.on("chat", (payload) => {
      setChat([...chat, payload]);
    });
  });
  return (
    <div className="App">
      <header className="App-header">
        <h1>Chatty Application</h1>

        {chat.map((data, index) => {
          return (
            <p key={index}>
              {data.message},{"            "} {data.userName}
            </p>
          );
        })}

        <form onSubmit={sendChat}>
          <input
            type="text"
            name="chat"
            placeholder="send text"
            value={message}
            onChange={(e) => {
              setMessage(e.target.value);
            }}
          />

          <button type="submit">Send</button>
        </form>
      </header>
    </div>
  );
}

export default App;
