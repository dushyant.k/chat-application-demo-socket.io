// const express = require("express");
// const app = express();
const app = require("express")();

const server = require("http").createServer(app);

const io = require("socket.io")(server, {
  cors: {
    origin: "*",
  },
});

// always you have to choose this name
io.on("connection", (socket) => {
  console.log("what is socket:", socket);
  console.log("socket is active to be connected");

  // you can put any name here
  socket.on("chat", (payload) => {
    console.log("what is payload in socket.on: ", payload);
    io.emit("chat", payload);
  });
});

server.listen(6464, () => {
  console.log("server connected at 6464");
});
